#pragma once
#include "disastrOS_constants.h"
#include "disastrOS_resource.h"
#include "linked_list.h"

typedef struct MSG{
  ListItem list;
  char buffer[MAX_MSG_BUFFERSIZE]; //buffer che contiene il msg
} MSG;

typedef struct MQ{
  Resource resource;
  ListHead ml; //lista di messaggi nella coda
} MQ;

void MSG_init();
MSG* MSG_alloc();
int MSG_free(MSG* msg);

void MQ_init();
MQ* MQ_alloc(int id, int type);
int MQ_free(MQ* mq);

