#include <assert.h>
#include <stdio.h>

#include "pool_allocator.h"
#include "disastrOS_resource.h"
#include "disastrOS.h"
#include "disastrOS_globals.h"
#include "disastrOS_constants.h"
#include "disastrOS_mqueue.h"

#define MSG_SIZE sizeof(MSG)
#define MSG_MEMSIZE (sizeof(MSG)+sizeof(int))
#define MAX_NUM_MSG (MAX_NUM_MSG_PER_MQUEUE*MAX_NUM_MQUEUE)
#define MSG_BUFFER_SIZE MAX_NUM_MSG*MSG_MEMSIZE

static char _msg_buffer[MSG_BUFFER_SIZE];
static PoolAllocator _msg_allocator;

#define MQ_SIZE sizeof(MQ)
#define MQ_MEMSIZE (sizeof(MQ)+sizeof(int))
#define MQ_BUFFER_SIZE MAX_NUM_MQUEUE*MQ_MEMSIZE

static char _mq_buffer[MQ_BUFFER_SIZE];
static PoolAllocator _mq_allocator;

void MSG_init(){
  int result=PoolAllocator_init(& _msg_allocator,
				MSG_SIZE,
				MAX_NUM_MSG,
				_msg_buffer,
				MSG_BUFFER_SIZE);
  assert(! result);
}

MSG* MSG_alloc() {
  MSG* msg=(MSG*)PoolAllocator_getBlock(&_msg_allocator);
  if (!msg)
    return 0;
  msg->list.prev=msg->list.next=0;
  return msg;
}

int MSG_free(MSG* msg) {
  return PoolAllocator_releaseBlock(&_msg_allocator, msg);
}


void MQ_init(){
  int result=PoolAllocator_init(& _mq_allocator,
				MQ_SIZE,
				MAX_NUM_MQUEUE,
				_mq_buffer,
				MQ_BUFFER_SIZE);
  assert(! result);
}

MQ* MQ_alloc(int id, int type) {
  MQ* mq=(MQ*)PoolAllocator_getBlock(&_mq_allocator);
  if (!mq)
    return 0;
  mq->resource.list.prev=mq->resource.list.next=0;
  mq->resource.id=id;
  mq->resource.type=type;
  List_init(&mq->resource.descriptors_ptrs);
  List_init(&mq->ml);
  return mq;
}

int MQ_free(MQ* mq) {
  int i;
  for(i=0; i < mq->ml.size; i++){
    MSG* msg= (MSG*)List_detach(&mq->ml, mq->ml.first);
    if (! msg){
      return -1;
    }
    MSG_free(msg);
  }
  return PoolAllocator_releaseBlock(&_mq_allocator, mq);
}
