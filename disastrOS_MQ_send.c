#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_resource.h"
#include "disastrOS_descriptor.h"
#include "disastrOS_mqueue.h"
#include "disastrOS_constants.h"

void internal_MQ_send(){

  char* buffer= (char*)running->syscall_args[0];
  int mq_fd= running->syscall_args[1];

  Descriptor* des=DescriptorList_byFd(&running->descriptors, mq_fd);
  if (! des){
    printf("errore nella ricerca del descrittore nella send\n");
    running->syscall_retvalue=DSOS_EMQSEND;
    return;
  }
  if(des->mode!=DSOS_WRITE){
    printf("Il processo %d non può scrivere sulla message queue!", des->pcb->pid);
    running->syscall_retvalue=DSOS_EMQSEND;
    return;
  }

  MQ* mq= (MQ*)des->resource;

  if(mq->ml.size==MAX_NUM_MSG_PER_MQUEUE){
    ListItem* waiting= (ListItem*)running;
    running= (PCB*)List_detach(&ready_list, ready_list.first);
    List_insert(&mq->ml, waiting_list.last, (ListItem*)waiting);
    return;
  }

  PCB* waiting_proc = NULL;
  ListItem* scroller = waiting_list.first;

  while(scroller){
    waiting_proc = (PCB*)scroller;
    if (waiting_proc->syscall_num == DSOS_CALL_MQ_RECEIVE){
      printf("ho trovato il processo con pid=%d che aspetta di ricevere un messaggio...\n", waiting_proc->pid);
      int waiting_fd = waiting_proc->syscall_args[1];
      Descriptor* wait_des = DescriptorList_byFd(&waiting_proc->descriptors, waiting_fd);
      Resource* wait_res = wait_des->resource;
      if (wait_res==des->resource){
        printf("il processo con pid=%d aspetta di ricevere un messaggio proprio dalla coda %d che ha aperto il processo running con pid=%d, perciò copiamo direttamente il buffer!\n", mq->resource.id, waiting_proc->pid, running->pid);
        List_detach(&waiting_list, (ListItem*) waiting_proc);
        printf("processo %d tolto dalla waiting list!\n", waiting_proc->pid);
        char* wait_msg = (char*) waiting_proc->syscall_args[0];
        waiting_proc->status = Ready;
        waiting_proc->syscall_retvalue = 0;
        strcpy(wait_msg, buffer);
        List_insert(&ready_list, ready_list.last, (ListItem*) waiting_proc);
        printf("processo %d messo nella ready list!\n", waiting_proc->pid);
        running->syscall_retvalue = 0;
        return;
      }
    }
    scroller=scroller->next;
  }

  MSG* msg= MSG_alloc();
  strcpy(msg->buffer, buffer);
  if(List_insert(&mq->ml, mq->ml.last, (ListItem*)msg)==0){
    printf("errore nell'inserimento del msg nella lista nella send\n");
    running->syscall_retvalue=DSOS_EMQSEND;
    return;
  }
  running->syscall_retvalue=0;

}